package com.example.practica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText nombretx,apellidotx,cedulatx;
    private Button ingresarbt,modificarbt,eliminarbt,guardarbt,cgeneralbt,cindividualbt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nombretx = (EditText)findViewById(R.id.nombretxt);
        apellidotx = (EditText)findViewById(R.id.apellidostxt);
        cedulatx = (EditText)findViewById(R.id.cedulatxt);
        ingresarbt = (Button)findViewById(R.id.insertarbtn);
        modificarbt = (Button)findViewById(R.id.modificarbtn);
        eliminarbt = (Button)findViewById(R.id.eliminarbtn);
        guardarbt = (Button)findViewById(R.id.guardarbtn);
        cgeneralbt = (Button)findViewById(R.id.consultageneralbtn);
        cindividualbt = (Button)findViewById(R.id.consultaindividualtxt);

         cgeneralbt.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

             }
         });

    }

    public void Registrar(View view){
        Sqlitehelper admin = new Sqlitehelper(this,"administracion",null,1);
        SQLiteDatabase Basededatos = admin.getWritableDatabase();

        String nombre=nombretx.getText().toString();
        String apellidos = apellidotx.getText().toString();
        String cedula= cedulatx.getText().toString();

        if(!cedula.isEmpty() && !nombre.isEmpty() && !apellidos.isEmpty()){
            ContentValues registros = new ContentValues();

            registros.put("Cedula",cedula);
            registros.put("Nombre",nombre);
            registros.put("apellidos",apellidos);

            Basededatos.insert("personas",null, registros);

            Basededatos.close();
            nombretx.setText("");
            apellidotx.setText("");
            cedulatx.setText("");
            Toast.makeText(this,"Los registro fueron guardados exitosamente ",Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(this, "Debes llenar todos los datos primero ",Toast.LENGTH_SHORT).show();
        }

    }

    public void Buscar (View view ){
        Sqlitehelper admin = new Sqlitehelper( this,"administracion",null,1);
        SQLiteDatabase Basededatos = admin.getWritableDatabase();

        String cedula=cedulatx.getText().toString();

        if(!cedula.isEmpty()){
            Cursor fila = Basededatos.rawQuery(
                    "select nombre, apellidos from personas where cedula ="+cedula,null );

            if(fila.moveToFirst()){
                nombretx.setText(fila.getString(0));
                apellidotx.setText(fila.getString(1));
                Basededatos.close();
            }else {
                Toast.makeText(this,"No existe el articulo",Toast.LENGTH_SHORT).show();
                Basededatos.close();
            }
        }else{
            Toast.makeText(this,"Debes introducir una cedula primero ",Toast.LENGTH_SHORT).show();
        }
    }

 



    public void eliminar (View view ){
        Sqlitehelper  admin = new Sqlitehelper
                (this,"administracion",null,1);
        SQLiteDatabase Basededatos=admin.getWritableDatabase();

        String cedula = cedulatx.getText().toString();


        if(!cedula.isEmpty()){

            int cantidad=Basededatos.delete("personas","cedula ="+cedula,null);
            Basededatos.close();

            cedulatx.setText("");
            nombretx.setText("");
            apellidotx.setText("");

            if(cantidad==1){
            Toast.makeText(this,"Articulo eliminado",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"debes introducir un codigo primer",Toast.LENGTH_SHORT).show();
            }


        }else{

            Toast.makeText(this,"Debes introducir un valor primero",Toast.LENGTH_SHORT).show();
        }
    }




        public void modificar(View view){
        Sqlitehelper admin = new Sqlitehelper (this,"administracion",null,1);
        SQLiteDatabase Basededatos = admin.getWritableDatabase();

        String cedula=cedulatx.getText().toString();
        String nombre=nombretx.getText().toString();
        String apellido=apellidotx.getText().toString();

        if(!cedula.isEmpty() && !nombre.isEmpty()&& !apellido.isEmpty()){

            ContentValues registro =new ContentValues();
            registro.put("cedula",cedula);
            registro.put("nombre",nombre);
            registro.put("apellidos",apellido);

            int cantidad=Basededatos.update("personas", registro, "cedula="+cedula,null);
            Basededatos.close();

            if(cantidad == 1){
                Toast.makeText(this,"datos modificados",Toast.LENGTH_SHORT).show();

            }else{
                Toast.makeText(this,"articulos no existes",Toast.LENGTH_SHORT).show();

            }


        }else{
            Toast.makeText(this,"debes llenar todos los campos",Toast.LENGTH_SHORT).show();
        }



        }







}
