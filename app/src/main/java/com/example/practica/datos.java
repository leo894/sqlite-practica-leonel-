package com.example.practica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class datos extends AppCompatActivity {

    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos);
        lv = findViewById(R.id.cg);

        List<datosx2> books = datosx2.(datosx2.class);
        List<String> listaTextoLibros = new ArrayList<>();
        for (Book libro: books){
            Log.e("Libro"
                    , libro.getTitle() + " " + libro.getEdition());
            listaTextoLibros.add(libro.getTitle() +" "+ libro.getEdition());
        }
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                listaTextoLibros);

        listalibros.setAdapter(itemsAdapter);
    }

    }
}
